
/**
 * Creates a new CanvasPresenter. The canvas presenter is responsible for drawing the mind map onto a
 * canvas view and reacting to user input on the map (e.g. dragging a node, double clicking it etc.)
 *
 * @constructor
 * @param {mindmaps.EventBus} eventBus
 * @param {mindmaps.CommandRegistry} commandRegistry
 * @param {mindmaps.MindMapModel} mindmapModel
 * @param {mindmaps.CanvasView} view
 * @param {mindmaps.ZoomController} zoomController
 */
mindmaps.CanvasPresenter = function(eventBus, commandRegistry, mindmapModel,
    view, zoomController) {
  var self = this;
  var creator = view.getCreator();
  var creatorEnabled = true;

  /**
   * Initializes this presenter.
   */
  this.init = function() {
    var editCaptionCommand = commandRegistry
        .get(mindmaps.EditNodeCaptionCommand);
    editCaptionCommand.setHandler(this.editNodeCaption.bind(this));

    var toggleNodeFoldedCommand = commandRegistry
        .get(mindmaps.ToggleNodeFoldedCommand);
    toggleNodeFoldedCommand.setHandler(toggleFold);

    /**
    * Disable all commands when overview mode is switched on.
    */
    eventBus.subscribe(mindmaps.Event.OVERVIEW_ENABLED, function()
    {
      editCaptionCommand.setEnabled(false);
      toggleNodeFoldedCommand.setEnabled(false);
    });

    /**
    * Enable all commands when overview mode is switched off.
    */
    eventBus.subscribe(mindmaps.Event.OVERVIEW_DISABLED,function()
    {
      editCaptionCommand.setEnabled(true);
      toggleNodeFoldedCommand.setEnabled(true);
    });
  };

  /**
   * Handles the edit caption command. Tells view to start edit mode for node.
   *
   * @param {mindmaps.Node} node
   */
  this.editNodeCaption = function(node) {
    if(creatorEnabled)
    {
      if (!node) {
        node = mindmapModel.selectedNode;
      }
      view.editNodeCaption(node);
    }
  };

  /**
   * Toggles the fold state of a node.
   *
   * @param {mindmaps.Node} node
   */
  var toggleFold = function(node) {
    if (!node) {
      node = mindmapModel.selectedNode;
    }

    // toggle node visibility
    var action = new mindmaps.action.ToggleNodeFoldAction(node);
    mindmapModel.executeAction(action);
  };

  /**
   * Tells the view to select a node.
   *
   * @param {mindmaps.Node} selectedNode
   * @param {mindmaps.Node} oldSelectedNode
   */
  var selectNode = function(selectedNode, oldSelectedNode) {

    // deselect old node
    if (oldSelectedNode) {
		if(typeof(oldSelectedNode.addChild) !== "undefined") {
			view.unhighlightNode(oldSelectedNode);
		} else {
			view.updateBranchColor(oldSelectedNode.getNode(),oldSelectedNode.getNode().branchColor, false);
		}
    }
    view.highlightNode(selectedNode);
  };

  /**
   * Tells the view to select a line.
   *
   * @param {mindmaps.Node} selectedLine
   * @param {mindmaps.Node} oldSelected
   */
	var selectLine = function(selectedLine, oldSelected){
		if(typeof(oldSelected.addChild) !== "undefined"){
			 view.unhighlightNode(oldSelected);
		} else {
			view.updateBranchColor(oldSelected.getNode(), oldSelected.getNode().branchColor, false);
		}
		view.updateBranchColor(selectedLine.getNode(), selectedLine.getNode().branchColor, true);
	}

  // listen to events from view
  /**
   * View callback: Zoom on mouse wheel.
   *
   * @ignore
   */
  view.mouseWheeled = function(delta) {
    view.stopEditNodeCaption();
    if (delta > 0) {
      zoomController.zoomIn();
    } else {
      zoomController.zoomOut();
    }
  };

  /**
   * View callback: Attach creator to node if mouse hovers over node.
   *
   * @ignore
   */
  view.nodeMouseOver = function(node) {
    if(creatorEnabled)
    {
      if (view.isNodeDragging() || creator.isDragging()) {
        // dont relocate the creator if we are dragging
      } else {
        creator.attachToNode(node);
      }
    }
  };

  /**
   * View callback: Attach creator to node if mouse hovers over node caption.
   *
   * @ignore
   */
  view.nodeCaptionMouseOver = function(node) {
    if(creatorEnabled)
    {
      if (view.isNodeDragging() || creator.isDragging()) {
        // dont relocate the creator if we are dragging
      } else {
        creator.attachToNode(node);
      }
    }
  };

  /**
   * View callback: Select node if mouse was pressed.
   *
   * @ignore
   */
  view.nodeMouseDown = function(node) {
    if(creatorEnabled)
    {
      mindmapModel.selectNode(node);
      // show creator
      creator.attachToNode(node);
    }
  };

   /**
   * View callback: Select line if mouse was pressed.
   *
   * @ignore
   */
  view.lineMouseDown = function(line){
    if(creatorEnabled)
    {
      mindmapModel.selectLine(line);
    }
  };
  // view.nodeMouseUp = function(node) {
  // };

  /**
   * View callback: Go into edit mode when node was double clicked.
   *
   * @ignore
   */
  view.nodeDoubleClicked = function(node) {
    if(creatorEnabled)
    {
      view.editNodeCaption(node);
    }
  };

  // view.nodeDragging = function() {
  // };

  /**
   * View callback: Execute MoveNodeAction when node was dragged.
   *
   * @ignore
   */
  view.nodeDragged = function(node, offset) {
    // view has updated itself

    // update model
    if(creatorEnabled)
    {
      var action = new mindmaps.action.MoveNodeAction(node, offset);
      mindmapModel.executeAction(action);
    }
  };

  /**
   * View callback: Toggle fold mode when fold button was clicked.
   *
   * @ignore
   */
  view.foldButtonClicked = function(node) {
    toggleFold(node);
  };

  // CREATOR TOOL
  /**
   * View callback: Return new random color to view when creator tool was
   * started to drag.
   *
   * @ignore
   */
  creator.dragStarted = function(node) {
    // set edge color for new node. inherit from parent or random when root
    var color = node.isRoot() ? mindmaps.Util.randomColor()
        : node.branchColor;
    return color;
  };

  /**
   * View callback: Create a new node when creator tool was stopped.
   *
   * @ignore
   */
  creator.dragStopped = function(parent, offsetX, offsetY, distance) {
    if(creatorEnabled)
    {
      // disregard if the creator was only dragged a bit
      if (distance < 50) {
        return;
      }

      // update the model
      var node = new mindmaps.Node();
      node.branchColor = creator.lineColor;
      node.offset = new mindmaps.Point(offsetX, offsetY);
      // indicate that we want to set this nodes caption after creation
      node.shouldEditCaption = true;
      mindmapModel.createNode(node, parent);
    }
  };

  /**
   * View callback: Change node caption when text change was committed in
   * view.
   *
   * @ignore
   * @param {mindmaps.Node} node
   * @param {String} str
   */
  view.nodeCaptionEditCommitted = function(node, str) {
    // avoid whitespace only strings
    var str = $.trim(str);
    if (!str) {
      return;
    }

    view.stopEditNodeCaption();
    mindmapModel.changeNodeCaption(node, str);
  };

  this.go = function() {
    view.init();
  };

  /**
   * Draw the mind map on the canvas.
   *
   * @param {mindmaps.Document} doc
   */
  function showMindMap(doc) {
    view.setZoomFactor(zoomController.DEFAULT_ZOOM);
    var dimensions = doc.dimensions;
    view.setDimensions(dimensions.x, dimensions.y);
    var map = doc.mindmap;
    view.drawMap(map);
    view.center();

    mindmapModel.selectNode(map.root);
  }

  /**
   * Hook up with EventBus.
   */
  function bind() {
    // listen to global events
    eventBus.subscribe(mindmaps.Event.DOCUMENT_OPENED, function(doc,
        newDocument) {
      showMindMap(doc);

      // if (doc.isNew()) {
      // // edit root node on start
      // var root = doc.mindmap.root;
      // view.editNodeCaption(root);
      // }
    });

    eventBus.subscribe(mindmaps.Event.DOCUMENT_CLOSED, function(doc) {
      view.clear();
    });

    eventBus.subscribe(mindmaps.Event.NODE_MOVED, function(node) {
      view.positionNode(node);
    });

    eventBus.subscribe(mindmaps.Event.NODE_TEXT_CAPTION_CHANGED, function(
        node) {
      view.setNodeText(node, node.getCaption());

      // redraw node in case height has changed
      // TODO maybe only redraw if height has changed
      view.redrawNodeConnectors(node);
    });

    eventBus.subscribe(mindmaps.Event.NODE_CREATED, function(node) {
      view.createNode(node);

      // edit node caption immediately if requested
      if (node.shouldEditCaption) {
        delete node.shouldEditCaption;
        // open parent node when creating a new child and the other
        // children are hidden
        var parent = node.getParent();
        if (parent.foldChildren) {
          var action = new mindmaps.action.OpenNodeAction(parent);
          mindmapModel.executeAction(action);
        }

        // select and go into edit mode on new node
        mindmapModel.selectNode(node);
        // attach creator manually, sometimes the mouseover listener wont fire
        creator.attachToNode(node);
        view.editNodeCaption(node);
      }
    });

    eventBus.subscribe(mindmaps.Event.NODE_DELETED, function(node, parent) {
      // select parent if we are deleting a selected node or a descendant
      var selected = mindmapModel.selectedNode;
      if (node === selected || node.isDescendant(selected)) {
        // deselectCurrentNode();
        mindmapModel.selectNode(parent);
      }

      // update view
      view.deleteNode(node);
      if (parent.isLeaf()) {
        view.removeFoldButton(parent);
      }
      view.removeCommentScreen(node);
    });

	  eventBus.subscribe(mindmaps.Event.LINE_SELECTED, selectLine);

    eventBus.subscribe(mindmaps.Event.NODE_SELECTED, selectNode);

    eventBus.subscribe(mindmaps.Event.NODE_OPENED, function(node) {
      view.openNode(node);
    });

    eventBus.subscribe(mindmaps.Event.NODE_CLOSED, function(node) {
      view.closeNode(node);
    });

    eventBus.subscribe(mindmaps.Event.NODE_FONT_CHANGED, function(node) {
      view.updateNode(node);
    });

	 eventBus.subscribe(mindmaps.Event.LINE_CHANGED, function(line) {
      view.updateNode(line.node, true);
    });

    eventBus.subscribe(mindmaps.Event.NODE_FONT_COLOR_PREVIEW, function(node, color) {
      view.updateFontColor(node, color);
    });

    eventBus.subscribe(mindmaps.Event.NODE_BRANCH_COLOR_CHANGED, function(
        node, selected) {
      view.updateNode(node, selected);
    });

	  eventBus.subscribe(mindmaps.Event.NODE_SIZE, function(node,width,height) {
      view.updateSize(node,width,height);
      view.updateNode(node,false);
    });

    eventBus.subscribe(mindmaps.Event.NODE_FONT_BACKGROUND, function(node, background) {
		var img2= new Image();

		img2.addEventListener('load',function(){
			//resize the picture
			if (img2.width==img2.height) {
				if (img2.width>200) {
				  img2.width=200;
				}
				if (  img2.height>200) {
				  img2.height=200;
				}
			} else {
				if (img2.width>img2.height) {
					if (img2.width>200) {
						var coef=200/img2.width;
						img2.width=200;
						img2.height=coef*img2.height;
					}
				} else if (img2.height>img2.width) {
					if (img2.height>200) {
						var coef=200/img2.height;
						img2.height=200;
						img2.width=coef*img2.width;
					}
				}
			}
			node.text.font.width=img2.width;
			node.text.font.height=img2.height;
			eventBus.publish(mindmaps.Event.NODE_SIZE, node,node.text.font.width, node.text.font.height);
			});
      view.updateNode(node,false);
	     img2.src=background;

      view.updateBackgroundColor(node, background);
    });

    eventBus.subscribe(mindmaps.Event.COMMENT_NODE, function(node,oldComment){
      if(!node.comment)
      {
        if(oldComment)
        {
          node.comment = oldComment
        }
        else
        {
          node.comment = "Insert your comment here";
        }
        view.drawCommentScreen(node);
      }
    });

	eventBus.subscribe(mindmaps.Event.COMMENT_LINE, function(line){
		if (!line.comment) {
			line.comment = true;
			view.updateLineComment(line);
		}
  });

  eventBus.subscribe(mindmaps.Event.LINK, function(node,node1) {
      view.createLink(node,node1);
    });

	eventBus.subscribe(mindmaps.Event.LINK_DEL, function(idNode,node) {
	  view.delLink(idNode,node);
	});

  eventBus.subscribe(mindmaps.Event.REMOVE_COMMENT_NODE, function(node)
  {
    if (node.comment) {
      view.removeCommentScreen(node);
      node.comment = false;
    }
  });
	eventBus.subscribe(mindmaps.Event.SUPPRIMER_COMENTAIRE_LIEN, function(line){
		line.comment = false;
		view.deleteLineComment(line);
  });

    eventBus.subscribe(mindmaps.Event.ZOOM_CHANGED, function(zoomFactor) {
      view.setZoomFactor(zoomFactor);
      view.applyViewZoom();
      view.scaleMap();
    });
	  eventBus.subscribe(mindmaps.Event.SUPPRIMER_COMENTAIRE_LIEN, function(line){
		  line.comment = false;
	  	view.deleteLineComment(line);
    });

    eventBus.subscribe(mindmaps.Event.NODE_BRANCH_COLOR_PREVIEW, function(node, color, selected) {
      view.updateBranchColor(node, color, selected)
    });

    eventBus.subscribe(mindmaps.Event.CENTER,function (node) {
      view.setcenter(node);
    });

    eventBus.subscribe(mindmaps.Event.ZOOM_CHANGED, function(zoomFactor) {
      view.setZoomFactor(zoomFactor);
      view.applyViewZoom();
      view.scaleMap();
    });

    setEnabled = function(e)
    {
      creatorEnabled = e;
    }

    /**
     * Disable creator functions, hide creator-nub, unhighlight currently selected node and make all nodes undraggable
     */
    eventBus.subscribe(mindmaps.Event.OVERVIEW_ENABLED, function()
    {
      setEnabled(false);
      $("#creator-wrapper").hide();
      view.unhighlightNode(mindmapModel.selected);
      view.makeNodeUndraggable();
    });

    /**
     * Enable creator functions, show creator-nub, rehighlight previously selected node and make all nodes draggable (except root)
     */
    eventBus.subscribe(mindmaps.Event.OVERVIEW_DISABLED, function()
    {
      setEnabled(true);
      $("#creator-wrapper").show();
      view.highlightNode(mindmapModel.selected);
      view.makeNodeDraggable();
    });
  }

  bind();
  this.init();
};
