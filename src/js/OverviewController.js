/**
 * @constructor
 * @param {mindmaps.EventBus} eventBus
 * @param {mindmaps.CommandRegistry} commandRegistry
 */
mindmaps.OverviewController = function(eventBus, commandRegistry) {
    var overviewCommand = commandRegistry.get(mindmaps.OverviewCommand);
    overviewCommand.setHandler(doOverview);
    mindmaps.OverviewController.pressed = false;

    /**
     * Handler of the overview command
     */
    function doOverview()
    {
        if(mindmaps.OverviewController.pressed == true)
        {
          mindmaps.OverviewController.pressed = false;
          eventBus.publish(mindmaps.Event.OVERVIEW_DISABLED)
        }
        else
        {
          if(mindmaps.OverviewController.pressed == false)
          {
            mindmaps.OverviewController.pressed = true ;
            eventBus.publish(mindmaps.Event.OVERVIEW_ENABLED)
          }
        }
        
    }
  
    /**
    * Disable controls when a document has been closed.
    */
    eventBus.subscribe(mindmaps.Event.DOCUMENT_CLOSED, function() {
      overviewCommand.setEnabled(false);
    });
  
    /**
     * Enable controls when a document has been opened.
     */
    eventBus.subscribe(mindmaps.Event.DOCUMENT_OPENED, function() {
      overviewCommand.setEnabled(true);
    });
};
  