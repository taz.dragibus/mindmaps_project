/**
 * Creates a new line.
 * 
 * @constructor
 */
mindmaps.Line = function($node) {  
	this.id = mindmaps.Util.getId();
	this.node = $node;
	this.comment = false;
	this.weight = $node.getDepth();
};

/**
 * Returns the node related to the line.
 * 
 * @returns {Node}
 */
mindmaps.Line.prototype.getNode = function(){
	return this.node;
}

/**
 * Returns the weight of the Line.
 * 
 * @returns {Integer}
 */
mindmaps.Line.prototype.getDepth = function(){
	return this.weight;
}