// TODO take container as argument,c reate drawing area dynamically. remove on
// clear();, recreate on init()

/**
 * Creates a new CanvasView. This is the base class for all canvas view
 * implementations.
 *
 * @constructor
 */
mindmaps.CanvasView = function() {
  /**
   * Returns the element that used to draw the map upon.
   *
   * @returns {jQuery}
   */
  this.$getDrawingArea = function() {
    return $("#drawing-area");
  };

  /**
   * Returns the element that contains the drawing area.
   *
   * @returns {jQuery}
   */
  this.$getContainer = function() {
    return $("#canvas-container");
  };

  /**
   * Centers the view on the node in parameters.
   *
   * @param {Node} node
   */
  this.setcenter = function(node) {
    var area = this.$getDrawingArea();
    var c = this.$getContainer();
    var w = area.width() - c.width();
    var h = area.height() - c.height();
    var delta = this.zoomFactor;
    var posx = node.offset.x*2.5;
    var posy = node.offset.y*2.5;
    while (node.getParent()!=node.getRoot() && !node.isRoot()) {
      node=node.getParent();
      posy = posy + node.offset.y*2.5;
      posx = posx + node.offset.x*2.5;
    }
    posx = posx*c.width()/4000;
    posy = posy*c.height()/2000;
    this.scroll(w/2+posx*delta,h/2+posy*delta);
  }
  /**
   * Scrolls the container to show the center of the drawing area.
   */
  this.center = function() {
    var c = this.$getContainer();
    var area = this.$getDrawingArea();
    var w = area.width() - c.width();
    var h = area.height() - c.height();
    this.scroll(w / 2, h / 2);
  };

  /**
   * Scrolls the container.
   *
   * @param {Number} x
   * @param {Number} y
   */
  this.scroll = function(x, y) {
    var c = this.$getContainer();
    c.scrollLeft(x).scrollTop(y);
  };

  /**
   * Changes the size of the drawing area to match with with the new zoom
   * factor and scrolls the container to adjust the view port.
   */
  this.applyViewZoom = function() {
    var delta = this.zoomFactorDelta;

    var c = this.$getContainer();
    var sl = c.scrollLeft();
    var st = c.scrollTop();

    var cw = c.width();
    var ch = c.height();
    var cx = cw / 2 + sl;
    var cy = ch / 2 + st;

    cx *= this.zoomFactorDelta;
    cy *= this.zoomFactorDelta;

    sl = cx - cw / 2;
    st = cy - ch / 2;

    var drawingArea = this.$getDrawingArea();
    var width = drawingArea.width();
    var height = drawingArea.height();
    drawingArea.width(width * delta).height(height * delta);

    // scroll only after drawing area's width was set.
    this.scroll(sl, st);

    // adjust background size
    var backgroundSize = parseFloat(drawingArea.css("background-size"));
    if (isNaN(backgroundSize)) {
      // parsing could possibly fail in the future.
      console.warn("Could not set background-size!");
    }
    drawingArea.css("background-size", backgroundSize * delta);
  };

  /**
   * Applies the new size according to current zoom factor.
   *
   * @param {Integer} width
   * @param {Integer} height
   */
  this.setDimensions = function(width, height) {
    width = width * this.zoomFactor;
    height = height * this.zoomFactor;

    var drawingArea = this.$getDrawingArea();
    drawingArea.width(width).height(height);
  };

  /**
   * Sets the new zoom factor and stores the delta to the old one.
   *
   * @param {Number} zoomFactor
   */
  this.setZoomFactor = function(zoomFactor) {
    this.zoomFactorDelta = zoomFactor / (this.zoomFactor || 1);
    this.zoomFactor = zoomFactor;
  };
};

/**
 * Should draw the mind map onto the drawing area.
 *
 * @param {mindmaps.MindMap} map
 */
mindmaps.CanvasView.prototype.drawMap = function(map) {
  throw new Error("Not implemented");
};

/**
 * Creates a new DefaultCanvasView. This is the reference implementation for
 * drawing mind maps.
 *
 * @extends mindmaps.CanvasView
 * @constructor
 */
mindmaps.DefaultCanvasView = function() {
  var self = this;
  var nodeDragging = false;
  var creator = new Creator(this);
  var captionEditor = new CaptionEditor(this);
  captionEditor.commit = function(node, text) {
    if (self.nodeCaptionEditCommitted) {
      self.nodeCaptionEditCommitted(node, text);
    }
  };

  var textMetrics = mindmaps.TextMetrics;
  var branchDrawer = new mindmaps.CanvasBranchDrawer();
  branchDrawer.beforeDraw = function(width, height, left, top) {
    this.$canvas.attr({
      width : width,
      height : height
    }).css({
      left : left,
      top : top
    });
  };

  /**
   * Enables dragging of the map with the mouse.
   */
  function makeDraggable() {
    self.$getContainer().dragscrollable({
      dragSelector : "#drawing-area, canvas.line-canvas",
      acceptPropagatedEvent : false,
      delegateMode : true,
      preventDefault : true
    });
  }

  /**
   * Enabled dragging for node (overview purpose)
   */
  this.makeNodeDraggable = function()
  {
    $(".node-container:not(.root)").draggable({disabled: false});
    $(".node-container").css({"opacity":1});
  }

  /**
   * Disable dragging for node (overview purpose)
  */
  this.makeNodeUndraggable = function()
  {
    $(".node-container:not(.root)").draggable({disabled: true});
    $(".node-container").css({"opacity":1});
  }
  
  /**
   * functions to get the different html elements 
   */
  function $getNodeCanvas(node) {
    return $("#line-canvas-" + node.id);
  }

  function $getLine(node) {
    return $(node.id);
  }

  function $getNodeDivCanvas(node) {
    return $("#line-canvas-div-" + node.id);
  }

  function $getNode(node) {
    return $("#node-" + node.id);
  }

  function $getNodeCaption(node) {
    return $("#node-caption-" + node.id);
  }

  function $getCommentScreen() {
    return $(".commentScreen");
  }

  function $getCommentArea() {
    return $(".commentArea");
  }
  
  function $getLinkCanvas(node, id) {
	return $("#node-canvasLink-"+id+"-"+node.id);
  }

  /**
   * Draw a new canvas corresponding to a line.
   *@param {canvas} $canvas
   *@param {int} depth
   *@param {int} offsetX
   *@param {int} offsetY
   *@param {Node} $node   
   *@param {Node} $parent
   *@param {Color} color
   *@param {boolean} selected
   */
  function drawLineCanvas($canvas, depth, offsetX, offsetY, $node, $parent,
      color, selected) {
    var canvas = $canvas[0];
    var ctx = canvas.getContext("2d");

    // set $canvas for beforeDraw() callback.
    branchDrawer.$canvas = $canvas;
    branchDrawer.render(ctx, depth, offsetX, offsetY, $node, $parent,
        color, self.zoomFactor, selected);

  }

  /**
   * Initialize the view.
   */
  this.init = function() {
    makeDraggable();
    this.center();

    var $drawingArea = this.$getDrawingArea();
    $drawingArea.addClass("mindmap");

    // setup delegates
    $drawingArea.delegate("div.node-caption", "mousedown", function(e) {
      var node = $(this).parent().data("node");
      if (self.nodeMouseDown) {
        self.nodeMouseDown(node);
      }
    });

	//select a line
	$drawingArea.delegate("canvas.line-canvas", "mousedown", function(e) {
      var line = $(this).data("line");
	  if(self.lineMouseDown){
		  self.lineMouseDown(line);
      }
    });

    $drawingArea.delegate("div.node-caption", "mouseup", function(e) {
      var node = $(this).parent().data("node");
      if (self.nodeMouseUp) {
        self.nodeMouseUp(node);
      }
    });

    $drawingArea.delegate("div.node-caption", "dblclick", function(e) {
      var node = $(this).parent().data("node");
      if (self.nodeDoubleClicked) {
        self.nodeDoubleClicked(node);
      }
    });

    $drawingArea.delegate("div.node-container", "mouseover", function(e) {
      if (e.target === this) {
        var node = $(this).data("node");
        if (self.nodeMouseOver) {
          self.nodeMouseOver(node);
        }
      }
      return false;
    });

    $drawingArea.delegate("div.node-caption", "mouseover", function(e) {
      if (e.target === this) {
        var node = $(this).parent().data("node");
        if (self.nodeCaptionMouseOver) {
          self.nodeCaptionMouseOver(node);
        }
      }
      return false;
    });

    // mouse wheel listener
    this.$getContainer().bind("mousewheel", function(e, delta) {
      if (self.mouseWheeled) {
        self.mouseWheeled(delta);
      }
    });
  };

  /**
   * Clears the drawing area.
   */
  this.clear = function() {
    var drawingArea = this.$getDrawingArea();
    drawingArea.children().remove();
    drawingArea.width(0).height(0);
  };

  /**
   * Calculates the width of a branch for a node for the given depth
   *
   * @param {Integer} depth the depth of the node
   * @returns {Number}
   */
  this.getLineWidth = function(depth) {
    return mindmaps.CanvasDrawingUtil.getLineWidth(this.zoomFactor, depth);
  };

  /**
   * Draws the complete map onto the drawing area. This should only be called
   * once for a mind map.
   */
  this.drawMap = function(map) {
    var now = new Date().getTime();
    var $drawingArea = this.$getDrawingArea();

    // clear map first
    $drawingArea.children().remove();

    var root = map.root;

    // 1.5. do NOT detach for now since DIV dont have widths and heights,
    // and loading maps draws wrong canvases (or create nodes and then draw
    // canvases)

    var detach = false;
    if (detach) {
      // detach drawing area during map creation to avoid unnecessary DOM
      // repaint events. (binary7 is 3 times faster)
      var $parent = $drawingArea.parent();
      $drawingArea.detach();
      self.createNode(root, $drawingArea);
      $drawingArea.appendTo($parent);
    } else {
      self.createNode(root, $drawingArea);
    }

    console.debug("draw map ms: ", new Date().getTime() - now);
  };

  /**
   * Inserts a new node including all of its children into the DOM.
   *
   * @param {mindmaps.Node} node - The model of the node.
   * @param {jQuery} [$parent] - optional jquery parent object the new node is
   *            appended to. Usually the parent node. If argument is omitted,
   *            the getParent() method of the node is called to resolve the
   *            parent.
   * @param {Integer} [depth] - Optional. The depth of the tree relative to
   *            the root. If argument is omitted the getDepth() method of the
   *            node is called to resolve the depth.
   */
  this.createNode = function(node, $parent, depth) {
    var parent = node.getParent();
    var $parent = $parent || $getNode(parent);

    var depth = depth || node.getDepth();
    var offsetX = node.offset.x;
    var offsetY = node.offset.y;

    // div node container
    var $node = $("<div/>", {
      id : "node-" + node.id,
      "class" : "node-container"
    }).data({
      node : node
    }).css({
      "font-size" : node.text.font.size
    });
    $node.appendTo($parent);

    if (node.isRoot()) {
      var w = this.getLineWidth(depth);
      $node.css("border-bottom-width", w);
    }

    if (!node.isRoot()) {
      // draw border and position manually only non-root nodes
      var bThickness = this.getLineWidth(depth);
      var bColor = node.branchColor;
      var bb = bThickness + "px solid " + bColor;

      $node.css({
        left : this.zoomFactor * offsetX,
        top : this.zoomFactor * offsetY,
        "border-bottom" : bb
      });

      // node drag behaviour
      /**
       * Only attach the drag handler once we mouse over it. this speeds
       * up loading of big maps.
       */
      $node.one("mouseenter", function() {
        $node.draggable({
          // could be set
          // revert: true,
          // revertDuration: 0,
          handle : "div.node-caption:first",
          start : function() {
            nodeDragging = true;
          },
          drag : function(e, ui) {
            // reposition and draw canvas while dragging
            var offsetX = ui.position.left / self.zoomFactor;
            var offsetY = ui.position.top / self.zoomFactor;
			
			var color = node.branchColor;
            var $canvas = $getNodeCanvas(node);

			$textarea = $("#line-caption-" + node.lineParent.id);
			$textarea.css("left", -(offsetX/2)*self.zoomFactor + "px");
			$textarea.css("top", -(offsetY/2)*self.zoomFactor + "px");

            drawLineCanvas($canvas, node.lineParent.weight, offsetX, offsetY, $node,$parent, color, false);
			
			
			//redraw all lines connected to the node
			var root = node.getRoot();
			
			node.falseChildren.forEach(function(child) {
              $node = $getNode(child);
              $canvas = $getLinkCanvas(node, node.falseChildren.indexOf(child));
              offsetX = child.getPosition().x - (ui.position.left / self.zoomFactor + node.getParent().getPosition().x / self.zoomFactor);
              offsetY = child.getPosition().y - (ui.position.top / self.zoomFactor + node.getParent().getPosition().y / self.zoomFactor);
				drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
            });

            //if the node is "child" of a symbolic link
            $node = $getNode(node);
            if(root.falseChildren.includes(node)){
              $parent = $getNode(root);
              $canvas = $getLinkCanvas(root, root.falseChildren.indexOf(node));
              offsetX = (ui.position.left / self.zoomFactor + node.getParent().getPosition().x) - root.getPosition().x;
			  offsetY = (ui.position.top / self.zoomFactor + node.getParent().getPosition().y) - root.getPosition().y;
				drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
            }
			root.forEachDescendant(function(parent) {
              if(parent.falseChildren.includes(node)){
                $parent = $getNode(parent);
                $canvas = $getLinkCanvas(parent, parent.falseChildren.indexOf(node));
                offsetX = (ui.position.left / self.zoomFactor + node.getParent().getPosition().x) - parent.getPosition().x;
                offsetY = (ui.position.top / self.zoomFactor + node.getParent().getPosition().y) - parent.getPosition().y;
					drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");              
				}
			});
			
            //redraw the child's node's symbolic links
            if (!node.isLeaf()){
              node.forEachDescendant(function(childNode){
                //if the node is "parent" of a symbolic link
                //$parent = $getNode(childNode);
                childNode.falseChildren.forEach(function(child) {
                  if (!node.isDescendant(child)){
                    if (parent == node){
                        $childNode = $getNode(child);
                        $canvas = $getLinkCanvas(childNode, childNode.falseChildren.indexOf(child));
                        offsetX = ((node.getPosition().x - childNode.getPosition().x) - (ui.position.left / self.zoomFactor)) + child.getPosition().x - node.getParent().getPosition().x;
                        offsetY = ((node.getPosition().y - childNode.getPosition().y) - (ui.position.top / self.zoomFactor)) + child.getPosition().y - node.getParent().getPosition().y;
							drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
                    } else {
                      $childNode = $getNode(child);
                      $canvas = $getLinkCanvas(childNode, childNode.falseChildren.indexOf(child));
                      offsetX = ((node.getPosition().x - childNode.getPosition().x) - (ui.position.left / self.zoomFactor)) + child.getPosition().x - node.getParent().getPosition().x;
                      offsetY = ((node.getPosition().y - childNode.getPosition().y) - (ui.position.top / self.zoomFactor)) + child.getPosition().y - node.getParent().getPosition().y;
						drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
                    }
                  }
                });

                //if the node is "child" of a symbolic link
                $childNode = $getNode(childNode);
                if(root.falseChildren.includes(childNode)){
                  $parent = $getNode(root);
                  $canvas = $getLinkCanvas(root, root.falseChildren.indexOf(childNode));
                  offsetX = ((ui.position.left / self.zoomFactor) - (node.getPosition().x - childNode.getPosition().x)) - root.getPosition().x + node.getParent().getPosition().x;
                  offsetY = ((ui.position.top / self.zoomFactor) - (node.getPosition().y - childNode.getPosition().y)) - root.getPosition().y + node.getParent().getPosition().y;
					drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
                }
                root.forEachDescendant(function(parent) {
                  if(parent.falseChildren.includes(childNode) && !node.isDescendant(parent)){
                    if (parent == node){
                      $parent = $getNode(parent);
                      $canvas = $getLinkCanvas(parent, parent.falseChildren.indexOf(childNode));
                      offsetX = ((ui.position.left / self.zoomFactor) - (node.getPosition().x - childNode.getPosition().x)) - ui.position.left + node.getParent().getPosition().x;
                      offsetY = ((ui.position.top / self.zoomFactor) - (node.getPosition().y - childNode.getPosition().y)) - ui.position.top + node.getParent().getPosition().y;
						drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
                    } else {
                      $parent = $getNode(parent);
                      $canvas = $getLinkCanvas(parent, parent.falseChildren.indexOf(childNode));
                      offsetX = ((ui.position.left / self.zoomFactor) - (node.getPosition().x - childNode.getPosition().x)) - parent.getPosition().x + node.getParent().getPosition().x;
                      offsetY = ((ui.position.top / self.zoomFactor) - (node.getPosition().y - childNode.getPosition().y)) - parent.getPosition().y + node.getParent().getPosition().y;
						drawLineCanvas($canvas, 1, offsetX, offsetY, $node, $parent, "#DCDCDC");
                    }
                  }
                });
              });
            }
			
            // fire dragging event
            if (self.nodeDragging) {
              self.nodeDragging();
            }
          },
          stop : function(e, ui) {
            nodeDragging = false;
            var pos = new mindmaps.Point(ui.position.left
                / self.zoomFactor, ui.position.top
                / self.zoomFactor);

            // fire dragged event
            if (self.nodeDragged) {
              self.nodeDragged(node, pos);
            }
          }
        });
      });
    }

    // text caption
    var font = node.text.font;
    var $text = $("<div/>", {
      id : "node-caption-" + node.id,
      "class" : "node-caption node-text-behaviour",
      text : node.text.caption
    }).css({
      "color" : font.color,
      "font-size" : this.zoomFactor * 100 + "%",
      "font-weight" : font.weight,
      "font-style" : font.style,
      "text-decoration" : font.decoration,
      "background-image": "url("+font.background+")",
      "height":font.height,
      "width": font.width,
      "background-size":"cover"
    }).appendTo($node);

    var metrics = textMetrics.getTextMetrics(node, this.zoomFactor);
    $text.css(metrics);

    // create fold button for parent if he hasn't one already
    var parentAlreadyHasFoldButton = $parent.data("foldButton");
    var nodeOrParentIsRoot = node.isRoot() || parent.isRoot();
    if (!parentAlreadyHasFoldButton && !nodeOrParentIsRoot) {
      this.createFoldButton(parent);
    }

    if (!node.isRoot()) {
      // toggle visibility
      if (parent.foldChildren) {
        $node.hide();
      } else {
        $node.show();
      }

      // draw canvas to parent if node is not a root
	  var line = new mindmaps.Line(node);


	  var $divCanvas = $("<div/>", {
        id : "line-canvas-div-" + node.id
      });

      var $canvas = $("<canvas/>", {
        id : "line-canvas-" + node.id,
        "class" : "line-canvas"
      }).data( {
	  line : line});


      // position and draw connection

      drawLineCanvas($canvas, depth, offsetX, offsetY, $node, $parent,
          node.branchColor, false);
      $canvas.appendTo($divCanvas);
	  $divCanvas.appendTo($node);
	  node.lineParent = line;
    }

    if (node.isRoot()) {
      $node.children().andSelf().addClass("root");
    }

    // draw child nodes
    node.forEachChild(function(child) {
      self.createNode(child, $node, depth + 1);
    });
  };

  /**
   * Removes a node from the view and with it all its children and the branch
   * leading to the parent.
   *
   * @param {mindmaps.Node} node
   */
  this.deleteNode = function(node) {
    // detach creator first, we need still him
    // creator.detach();

    // delete all DOM below
    var $node = $getNode(node);
    $node.remove();
  };

  /**
   * Highlights a node to show that it is selected.
   *
   * @param {mindmaps.Node} node
   */
  this.highlightNode = function(node) {
    var $text = $getNodeCaption(node);
    $text.addClass("selected");
    if(node.comment)
    {
      this.drawCommentScreen(node);
    }
  };

  /**
   * Removes the highlight of a node.
   *
   * @param {mindmaps.Node} node
   */
  this.unhighlightNode = function(node) {
    var $text = $getNodeCaption(node);
    $text.removeClass("selected");
    if(node.comment)
    {
      this.removeCommentScreen(node);
    }
  };

  /**
   * Hides all children of a node.
   *
   * @param {mindmaps.Node} node
   */
  this.closeNode = function(node) {
    var $node = $getNode(node);
    $node.children(".node-container").hide();

    var $foldButton = $node.children(".button-fold").first();
    $foldButton.removeClass("open").addClass("closed");
  };

  /**
   * Shows all children of a node.
   *
   * @param {mindmaps.Node} node
   */
  this.openNode = function(node) {
    var $node = $getNode(node);
    $node.children(".node-container").show();

    var $foldButton = $node.children(".button-fold").first();
    $foldButton.removeClass("closed").addClass("open");
  };

  /**
   * Creates the fold button for a node that shows/hides its children.
   *
   * @param {mindmaps.Node} node
   */
  this.createFoldButton = function(node) {
    var position = node.offset.x > 0 ? " right" : " left";
    var openClosed = node.foldChildren ? " closed" : " open";
    var $foldButton = $("<div/>", {
      "class" : "button-fold no-select" + openClosed + position
    }).click(function(e) {
      // fire event
      if (self.foldButtonClicked) {
        self.foldButtonClicked(node);
      }

      e.preventDefault();
      return false;
    });

    // remember that foldButton was set and attach to node
    var $node = $getNode(node);
    $node.data({
      foldButton : true
    }).append($foldButton);
  };

  /**
   * Removes the fold button.
   *
   * @param {mindmaps.Node} node
   */
  this.removeFoldButton = function(node) {
    var $node = $getNode(node);
    $node.data({
      foldButton : false
    }).children(".button-fold").remove();
  };

  /**
   * Goes into edit mode for a node.
   *
   * @param {mindmaps.Node} node
   */
  this.editNodeCaption = function(node) {
    captionEditor.edit(node, this.$getDrawingArea());
  };

  /**
   * Stops the current edit mode.
   */
  this.stopEditNodeCaption = function() {
    captionEditor.stop();
  };

  /**
   * Updates the text caption for a node.
   *
   * @param {mindmaps.Node} node
   * @param {String} value
   */
  this.setNodeText = function(node, value) {
    var $text = $getNodeCaption(node);
    var metrics = textMetrics.getTextMetrics(node, this.zoomFactor, value);
    $text.css(metrics).text(value);
  };

  /**
   * Get a reference to the creator tool.
   *
   * @returns {Creator}
   */
  this.getCreator = function() {
    return creator;
  };

  /**
   * Returns whether a node is currently being dragged.
   *
   * @returns {Boolean}
   */
  this.isNodeDragging = function() {
    return nodeDragging;
  };

  /**
   * Redraws a node's branch to its parent.
   *
   * @param {mindmaps.Node} node
   * @param {String} optional color
   */
  function drawNodeCanvas(node, color, selected) {

    var parent = node.getParent();
    var depth = node.lineParent.getDepth();
    var offsetX = node.offset.x;
    var offsetY = node.offset.y;
    color = color || node.branchColor;

    var $node = $getNode(node);
    var $parent = $getNode(parent);
    var $canvas = $getNodeCanvas(node);
	if(selected){
		select2 = true;
	}
	else {
		select2 = false;
	}
    drawLineCanvas($canvas, depth, offsetX, offsetY, $node, $parent, color, select2);
 }

  /**
   * Redraws all branches that a node is connected to.
   *
   * @param {mindmaps.Node} node
   */
  this.redrawNodeConnectors = function(node, selected) {

    // redraw canvas to parent
    if (!node.isRoot()) {
      drawNodeCanvas(node, node.branchColor, selected);
    }

    // redraw all child canvases
    if (!node.isLeaf()) {
      node.forEachChild(function(child) {
        drawNodeCanvas(child, node.branchColor, selected);
      });
    }
  };

  /**
   * Changes only the color of the branch leading up to it's parent.
   */
  this.updateBranchColor = function(node, color, selected) {
    var $node = $getNode(node);
    $node.css("border-bottom-color", color);
	if(selected){
		select2 = true;
	}
	else {
		select2 = false;
	}
    // redraw canvas to parent
    if (!node.isRoot()) {
      drawNodeCanvas(node, color, select2);
    }
  };

  /**
   * Changes only the font color of a node.
   */
  this.updateFontColor = function(node, color) {
    var $text = $getNodeCaption(node);
    $text.css("color", color);
  };

   this.updateBackgroundColor = function(node, background) {

	var $text = $getNodeCaption(node);

	$text.css("background-image", "url('"+background+"')");
  $text.css("background-size", "cover");

  var metrics = textMetrics.getTextMetrics(node, this.zoomFactor);
	$text.css(metrics);
  };

  /**
  * Update the size of a Node
  * @param {Node} node
  * @param {int} width
  * @param {int} height
  */
  this.updateSize = function(node,width,height) {
    width = node.text.font.width;
    height = node.text.font.height;
    var $text = $getNodeCaption(node);
    $text.css("width", width + "px");
    $text.css("height", height + "px");
  };

  /** 
  * Draw the comment screen of a node
  * @param {Node} node
  */
  this.drawCommentScreen = function(node)
  {
    var $commentScreen = $("<div/>", {"class" : "commentScreen float-panel ui-dialog ui-widget-content"})
    .css({"z-index" : 1000, "position" : "fixed", "bottom" : 50 + 200 * this.zoomFactor + "px", "left" : 20 + "px", "font-size" : "12px", "width" : 200 * this.zoomFactor + "px", "height" : 200 * this.zoomFactor + "px", "backgroundColor" : "white", "border-radius" : 3 + "px"})
    .appendTo(this.$getContainer())

    var $commentScreenTitle = $("<h1/>", {"class" : "ui-dialog ui-dialog-titlebar ui-widget-header", text : "Comment"} )
    .css({"position" : "absolute", "box-shadow" : "none"})
    .appendTo($commentScreen);

    var $commentTextArea = $("<textarea/>", {"class" : "commentArea .ui-widget-content", text : node.comment})
    .css({ "resize" : "none", "width" : 192+"px", "height" : 168  +"px", "margin-top" : "25px"})
    .appendTo($commentScreen);

    $commentTextArea.bind('blur',function()
    {
      node.comment = $commentTextArea.val();
    });
  }

  /**
  * Removes the comment screen of a node
  * @param {Node} node
  */
  this.removeCommentScreen = function(node)
  {
    node.comment = $getCommentArea().val();
    $getCommentScreen().remove();
  }
  
  /**
  * create a symbolic node between two nodes 
  * @param {Node} node
  * @param {Node} node1
  */
  this.createLink = function (node, node1){
      var number = node.falseChildren.length - 1;
      var $canvasLink = $("<canvas/>", {
        id : "node-canvasLink-" + number + "-" + node.id,
        "class" : "line-canvas"
      });

      var $node = $getNode(node);
      var $node1 = $getNode(node1);

      var offsetX = node1.getPosition().x - node.getPosition().x;
      var offsetY = node1.getPosition().y - node.getPosition().y;

      drawLineCanvas($canvasLink, 1, offsetX, offsetY, $node, $node1, "#DCDCDC");

      $canvasLink.appendTo($node1);
  };
  
  /**
  * remove a symbolic node between two nodes 
  * @param {int} idNode
  * @param {Node} node
  */
  this.delLink = function(idNode,node){
	  var $canvas = $getLinkCanvas(node, idNode);
	  $canvas.remove();
  };

  /**
  * updates the comment on a line
  * @param {Line} line
  */
  this.updateLineComment = function(line){
	  if(line.comment){
		var $text = $("<textarea/>", {
		  id : "line-caption-" + line.id,
		  "class" : "line-caption",
		  text : "Insert your comment here",
		}).css({
			"left" : -(line.node.offset.x/2)*this.zoomFactor + "px",
			"top" : -(line.node.offset.y/2)*this.zoomFactor + "px",
			"color" : "black",
			"font-size" : this.zoomFactor * 100 + "%",
			"width" : 150 * this.zoomFactor + "px",
			"height" : 50 * this.zoomFactor + "px",
			"border" : "1px dotted" + line.node.branchColor
		}).appendTo($getNodeDivCanvas(line.node));
	  }
  }
  
  /**
  * updates the position of the comment on a line
  * @param {Line} line
  */
  this.updateLineCommentPosition = function(line){
	  	$textarea = $("#line-caption-" + line.id);
		$textarea.css("left", -(line.node.offset.x/2)*this.zoomFactor + "px");
		$textarea.css("top", -(line.node.offset.y/2)*this.zoomFactor + "px");
		$textarea.css("font-size", 100*this.zoomFactor + "%");
		$textarea.css("width", 150*this.zoomFactor + "px");
		$textarea.css("height", 50*this.zoomFactor + "px");

  }

  /**
  * deletes the comment on a line
  * @param {Line} line
  */
  this.deleteLineComment = function(line) {
	  	$textarea = $("#line-caption-" + line.id);
		$textarea.remove();
  }

  /**
   * Does a complete visual update of a node to reflect all of its attributes.
   *
   * @param {mindmaps.Node} node
   */
  this.updateNode = function(node, selected) {
    var $node = $getNode(node);
    var $text = $getNodeCaption(node);
    var font = node.text.font;
	if(node.lineParent != undefined){
		var w = this.getLineWidth(node.lineParent.weight);
	}
	else {
		var w = this.getLineWidth(1);
	}

    $node.css({
      "font-size" : font.size,
      "border-bottom-color" : node.branchColor,
	  "border-bottom-width" : w
    });

    var metrics = textMetrics.getTextMetrics(node, this.zoomFactor);

    $text.css({
      "color" : font.color,
      "font-weight" : font.weight,
      "font-style" : font.style,
      "text-decoration" : font.decoration,
      "background-image" : font.background
    }).css(metrics);
    this.redrawNodeConnectors(node, selected);
	if(node.lineParent != undefined && node.lineParent.comment){
		this.updateLineCommentPosition(node.lineParent);
	}
  };

  /**
   * Moves the node a new position.
   *
   * @param {mindmaps.Node} node
   */
  this.positionNode = function(node) {
    var $node = $getNode(node);
    // TODO try animate
    // position
    $node.css({
      left : this.zoomFactor * node.offset.x,
      top : this.zoomFactor * node.offset.y
    });

    // redraw canvas to parent
    drawNodeCanvas(node);

	this.updateLineCommentPosition(node.lineParent);
  };

  /**
   * Redraws the complete map to adapt to a new zoom factor.
   */
  this.scaleMap = function() {
    var zoomFactor = this.zoomFactor;
    var $root = this.$getDrawingArea().children().first();
    var root = $root.data("node");

    var w = this.getLineWidth(0);
    $root.css("border-bottom-width", w);

    // handle root differently
    var $text = $getNodeCaption(root);
    var metrics = textMetrics.getTextMetrics(root, this.zoomFactor);
    $text.css(
        {
          "font-size" : zoomFactor * 100 + "%",
          "left" : zoomFactor
              * -mindmaps.TextMetrics.ROOT_CAPTION_MIN_WIDTH / 2
        }).css(metrics);

    root.forEachChild(function(child) {
      scale(child, 1);
    });

    function scale(node, depth) {
      var $node = $getNode(node);

      // draw border and position manually
      var bWidth = self.getLineWidth(node.lineParent.weight);

      $node.css({
        left : zoomFactor * node.offset.x,
        top : zoomFactor * node.offset.y,
        "border-bottom-width" : bWidth
      });

      var $text = $getNodeCaption(node);
      $text.css({
        "font-size" : zoomFactor * 100 + "%"
      });

	  var $textarea = $("#line-caption-" + node.lineParent.id);
	   
	  self.updateLineCommentPosition(node.lineParent);
      var metrics = textMetrics.getTextMetrics(node, self.zoomFactor);
      $text.css(metrics);

      // redraw canvas to parent
      drawNodeCanvas(node);
      // redraw all child canvases
      if (!node.isLeaf()) {
        node.forEachChild(function(child) {
          scale(child, depth + 1);
        });
      }
    }
  };

  /**
   * Creates a new CaptionEditor. This tool offers an inline editor component
   * to change a node's caption.
   *
   * @constructor
   * @param {mindmaps.CanvasView} view
   */
  function CaptionEditor(view) {
    var self = this;
    var attached = false;

    // text input for node edits.
    var $editor = $("<textarea/>", {
      id : "caption-editor",
      "class" : "node-text-behaviour"
    }).bind("keydown", "esc", function() {
      self.stop();
    }).bind("keydown", "return", function() {
      commitText();
    }).mousedown(function(e) {
      // avoid premature canceling
      e.stopPropagation();
    }).blur(function() {
      commitText();
    }).bind(
        "input",
        function() {
          var metrics = textMetrics.getTextMetrics(self.node,
              view.zoomFactor, $editor.val());
          $editor.css(metrics);
          alignBranches();
        });

    function commitText() {
      if (attached && self.commit) {
        self.commit(self.node, $editor.val());
      }
    }

    function alignBranches() {
      // slightly defer execution for better performance on slow
      // browsers
      setTimeout(function() {
        view.redrawNodeConnectors(self.node);
      }, 1);
    }

    /**
     * Attaches the textarea to the node and temporarily removes the
     * original node caption.
     *
     * @param {mindmaps.Node} node
     * @param {jQuery} $cancelArea
     */
    this.edit = function(node, $cancelArea) {
      if (attached) {
        return;
      }
      this.node = node;
      attached = true;

      // TODO put text into span and hide()
      this.$text = $getNodeCaption(node);
      this.$cancelArea = $cancelArea;

      this.text = this.$text.text();

      this.$text.css({
        width : "auto",
        height : "auto"
      }).empty().addClass("edit");

      // jquery ui prevents blur() event from happening when dragging a
      // draggable. need this
      // workaround to detect click on other draggable
      $cancelArea.bind("mousedown.editNodeCaption", function(e) {
        commitText();
      });

      var metrics = textMetrics.getTextMetrics(self.node,
          view.zoomFactor, this.text);
      $editor.attr({
        value : this.text
      }).css(metrics).appendTo(this.$text).select();

    };

    /**
     * Removes the editor from the node and restores its old text value.
     */
    this.stop = function() {
      if (attached) {
        attached = false;
        this.$text.removeClass("edit");
        $editor.detach();
        this.$cancelArea.unbind("mousedown.editNodeCaption");
        view.setNodeText(this.node, this.text);

        //alignBranches();
      }
    };
  }

  /**
   * Creates a new Creator. This tool is used to drag out new branches to
   * create new nodes.
   *
   * @constructor
   * @param {mindmaps.CanvasView} view
   * @returns {Creator}
   */
  function Creator(view) {
    var self = this;
    var dragging = false;

    this.node = null;
    this.lineColor = null;

    var $wrapper = $("<div/>", {
      id : "creator-wrapper"
    }).bind("remove", function(e) {
      // detach the creator when some removed the node or opened a new map
      self.detach();
      // and avoid removing from DOM
      e.stopImmediatePropagation();

      console.debug("creator detached.");
      return false;
    });

    // red dot creator element
    var $nub = $("<div/>", {
      id : "creator-nub"
    }).appendTo($wrapper);

    var $fakeNode = $("<div/>", {
      id : "creator-fakenode"
    }).appendTo($nub);

    // canvas used by the creator tool to draw new lines
    var $canvas = $("<canvas/>", {
      id : "creator-canvas",
      "class" : "line-canvas"
    }).hide().appendTo($wrapper);

    // make it draggable
    $wrapper.draggable({
      revert : true,
      revertDuration : 0,
      start : function() {
        dragging = true;
        // show creator canvas
        $canvas.show();
        if (self.dragStarted) {
          self.lineColor = self.dragStarted(self.node);
        }
      },
      drag : function(e, ui) {
        // update creator canvas
        var offsetX = ui.position.left / view.zoomFactor;
        var offsetY = ui.position.top / view.zoomFactor;

        // set depth+1 because we are drawing the canvas for the child
        var $node = $getNode(self.node);
        drawLineCanvas($canvas, self.depth + 1, offsetX, offsetY,
            $fakeNode, $node, self.lineColor, false);
      },
      stop : function(e, ui) {
        dragging = false;
        // remove creator canvas, gets replaced by real canvas
        $canvas.hide();
        if (self.dragStopped) {
          var $wp = $wrapper.position();
          var $wpLeft = $wp.left / view.zoomFactor;
          var $wpTop = $wp.top / view.zoomFactor;
          var nubLeft = ui.position.left / view.zoomFactor;
          var nubTop = ui.position.top / view.zoomFactor;

          var distance = mindmaps.Util.distance($wpLeft - nubLeft,
              $wpTop - nubTop);
          self.dragStopped(self.node, nubLeft, nubTop, distance);
        }

        // remove any positioning that the draggable might have caused
        $wrapper.css({
          left : "",
          top : ""
        });
      }
    });

    /**
     * Attaches the tool to a node.
     *
     * @param {mindmaps.Node} node
     */
    this.attachToNode = function(node) {
      if (this.node === node) {
        return;
      }
      this.node = node;

      // position the nub correctly
      $wrapper.removeClass("left right");
      if (node.offset.x > 0) {
        $wrapper.addClass("right");
      } else if (node.offset.x < 0) {
        $wrapper.addClass("left");
      }

      // set border on our fake node for correct line drawing
      this.depth = node.getDepth();
      var w = view.getLineWidth(this.depth + 1);
      $fakeNode.css("border-bottom-width", w);

      var $node = $getNode(node);
      $wrapper.appendTo($node);
    };

    /**
     * Removes the tool from the current node.
     */
    this.detach = function() {
      $wrapper.detach();
      this.node = null;
    };

    /**
     * Returns whether the tool is currently in use being dragged.
     *
     * @returns {Boolean}
     */
    this.isDragging = function() {
      return dragging;
    };
  }
};

// inherit from base canvas view
mindmaps.DefaultCanvasView.prototype = new mindmaps.CanvasView();
