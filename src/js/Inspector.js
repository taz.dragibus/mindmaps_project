/**
 * Creates a new InspectorView.
 *
 * @constructor
 */
mindmaps.InspectorView = function() {
  var self = this;
  var $content = $("#template-inspector").tmpl();

  var $importImage = $("#inspector-button-import-image",$content);
  var $linkImage = $("#inspector-button-link-image",$content);
  var $delImage = $("#inspector-button-del-image",$content);

  var $addComment = $("#comment", $content);
  var $delComment = $("#delComment", $content);
  
  var $center = $("#center-button",$content);
  var $centerSelected = $("#center-selected-button",$content);
  var $search = $("#search-button", $content);

  var $addLink = $("#addLink",$content);
  var $delLink = $("#delLink",$content);
  
  var $link = $("#link_tr", $content);

  var $sizeDecreaseButton = $("#inspector-button-font-size-decrease",$content);
  var $sizeIncreaseButton = $("#inspector-button-font-size-increase",$content);
  var $boldCheckbox = $("#inspector-checkbox-font-bold", $content);
  var $italicCheckbox = $("#inspector-checkbox-font-italic", $content);
  var $underlineCheckbox = $("#inspector-checkbox-font-underline", $content);
  var $linethroughCheckbox = $("#inspector-checkbox-font-linethrough",$content);
  var $branchColorChildrenButton = $("#inspector-button-branch-color-children", $content);
  
  var branchColorPicker = $("#inspector-branch-color-picker", $content);
  var fontColorPicker = $("#inspector-font-color-picker", $content);
  
  var $allButtons = [$importImage, $linkImage, $delImage, $addComment, $delComment, $center, $centerSelected, $search, $addLink, $delLink,
      $sizeDecreaseButton, $sizeIncreaseButton,
      $boldCheckbox, $italicCheckbox, $underlineCheckbox,
      $linethroughCheckbox, $branchColorChildrenButton ];
  
  var $allColorpickers = [ branchColorPicker, fontColorPicker ];

  var $addPicture = $("#addPicture", $content);
  var $font_style = $("#font_style", $content);
  var $font_color = $("#font_color", $content);
  var $tr_center = $("#tr_center", $content);

  /**
   * Returns a jquery object.
   *
   * @returns {jQuery}
   */
  this.getContent = function() {
    return $content;
  };

  /**
   * Sets the enabled state of all controls.
   *
   * @param {Boolean} enabled
   */
  this.setControlsEnabled = function(enabled) {
    var state = enabled ? "enable" : "disable";
    $allButtons.forEach(function($button) {
      $button.button(state);
    });

    $allColorpickers.forEach(function($colorpicker) {
      $colorpicker.miniColors("disabled", !enabled);
    });
  };

  /**
   * Sets the checked state of the bold font option.
   *
   * @param {Boolean} checked
   */
  this.setBoldCheckboxState = function(checked) {
    $boldCheckbox.prop("checked", checked).button("refresh");
  };

  /**
   * Sets the checked state of the italic font option.
   *
   * @param {Boolean} checked
   */
  this.setItalicCheckboxState = function(checked) {
    $italicCheckbox.prop("checked", checked).button("refresh");
  };

  /**
   * Sets the checked state of the underline font option.
   *
   * @param {Boolean} checked
   */
  this.setUnderlineCheckboxState = function(checked) {
    $underlineCheckbox.prop("checked", checked).button("refresh");
  };

  /**
   * Sets the checked state of the linethrough font option.
   *
   * @param {Boolean} checked
   */
  this.setLinethroughCheckboxState = function(checked) {
    $linethroughCheckbox.prop("checked", checked).button("refresh");
  };

  /**
   * Sets the color of the branch color picker.
   *
   * @param {String} color
   */
  this.setBranchColorPickerColor = function(color) {
    branchColorPicker.miniColors("value", color);
  };

  /**
   * Sets the color of the font color picker.
   *
   * @param {String} color
   */
  this.setFontColorPickerColor = function(color) {
    fontColorPicker.miniColors("value", color);
  };

  /**
   * sets the inspector in the view related to a line
   */
  this.lineSelected = function(){
    $addPicture.css("display", "none");
	$font_style.css("display", "none");
	$font_color.css("display", "none");
	$link.css("display", "none");
	$centerSelected.css("display", "none");

	var strMessage1 = document.getElementById("size_line") ;
	var html = strMessage1.innerHTML;
	html = html.replace( "Font","Line");
	strMessage1.innerHTML = html;
  };

  /**
   * sets the inspector in the view related to a node
   */
  this.nodeSelected = function(){
    $addPicture.css("display", "table-row");
    $font_style.css("display", "table-row");
	$font_color.css("display", "table-row");
	$centerSelected.css("display", "inline-block");
	$link.css("display", "table-row");
	var strMessage1 = document.getElementById("size_line") ;
	var html = strMessage1.innerHTML;
	html = html.replace( "Line","Font");
	strMessage1.innerHTML = html;
  };

  /**
   * Initialize
   */
  this.init = function() {
    $(".buttonset", $content).buttonset();
    $branchColorChildrenButton.button();

	$addLink.click(function(){
		if(self.addLinkClicked){
			self.addLinkClicked();
		}
	});

	$delLink.click(function(){
		if(self.delLinkClicked){
			self.delLinkClicked();
		}
	});

  $center.click(function(){
  	if(self.centerClicked){
  		self.centerClicked();
  	}
  });
  
  $centerSelected.click(function(){
	if(self.centerSelectedClicked){
		self.centerSelectedClicked();
	}
  });

	$search.click(function(){
  		if(self.searchClicked){
  			self.searchClicked();
  		}
  	});

	$addComment.click(function(){
		if(self.addCommentClicked){
			self.addCommentClicked();
		}
	});

	$delComment.click(function(){
		if(self.delCommentClicked){
			self.delCommentClicked();
		}
	});

    $linkImage.click(function(){
      if(self.linkImageClicked){
        self.linkImageClicked();
      }
    });

    $delImage.click(function(){
      if(self.delImageClicked){
        self.delImageClicked();
      }
    });

  $importImage.bind("change", function(e){
    if(self.importImageClicked){
      self.importImageClicked(e);
    }
  });

  $("#inspector-button-font-size-decrease").live('click', function(e) {
      if (self.fontSizeDecreaseButtonClicked) {
        self.fontSizeDecreaseButtonClicked();
      }
    });

    $("#inspector-button-font-size-increase").live('click', function(e) {
      if (self.fontSizeIncreaseButtonClicked) {
        self.fontSizeIncreaseButtonClicked();
      }
    });

    $boldCheckbox.click(function() {
      if (self.fontBoldCheckboxClicked) {
        var checked = $(this).prop("checked");
        self.fontBoldCheckboxClicked(checked);
      }
    });

    $italicCheckbox.click(function() {
      if (self.fontItalicCheckboxClicked) {
        var checked = $(this).prop("checked");
        self.fontItalicCheckboxClicked(checked);
      }
    });

    $underlineCheckbox.click(function() {
      if (self.fontUnderlineCheckboxClicked) {
        var checked = $(this).prop("checked");
        self.fontUnderlineCheckboxClicked(checked);
      }
    });

    $linethroughCheckbox.click(function() {
      if (self.fontLinethroughCheckboxClicked) {
        var checked = $(this).prop("checked");
        self.fontLinethroughCheckboxClicked(checked);
      }
    });

    branchColorPicker.miniColors({
      hide : function(hex) {
        // dont emit event if picker was hidden due to disable
        if (this.attr('disabled')) {
          return;
        }

        console.log("hide branch", hex);
        if (self.branchColorPicked) {
          self.branchColorPicked(hex);
        }
      },

      move : function(hex) {
        if (self.branchColorPreview) {
          self.branchColorPreview(hex);
        }
      }
    });

    fontColorPicker.miniColors({
      hide : function(hex) {
        // dont emit event if picker was hidden due to disable
        if (this.attr('disabled')) {
          return;
        }
        console.log("font", hex);
        if (self.fontColorPicked) {
          self.fontColorPicked(hex);
        }
      },

      move: function(hex) {
        if (self.fontColorPreview) {
          self.fontColorPreview(hex);
        }
      }
    });

    $branchColorChildrenButton.click(function() {
      if (self.branchColorChildrenButtonClicked) {
        self.branchColorChildrenButtonClicked();
      }
    });
  };
};

/**
 * Creates a new InspectorPresenter. This presenter manages basic node
 * attributes like font settings and branch color.
 *
 * @constructor
 * @param {mindmaps.EventBus} eventBus
 * @param {mindmaps.MindMapModel} mindmapModel
 * @param {mindmaps.InspectorView} view
 */
mindmaps.InspectorPresenter = function(eventBus, mindmapModel, view) {
  var self = this;
  var oldNode = null;
  
  /**
   * View callbacks: React to user input and execute appropiate action.
   */
   view.linkImageClicked = function() {
    var img = prompt("picture url : ","type here");
    if (img!=null) {
		var action = new mindmaps.action.linkImageAction(mindmapModel.selected,img);
		mindmapModel.executeAction(action);
    }
   };

   view.centerClicked = function() {
	   if(mindmapModel.selected.getRoot){
			var action = new mindmaps.action.centerAction(mindmapModel.selected.getRoot());
	   }
	   else {
			var action = new mindmaps.action.centerAction(mindmapModel.selected.node.getRoot());
	   }
		mindmapModel.executeAction(action);
   }
   
   view.centerSelectedClicked = function() {
		var action = new mindmaps.action.centerAction(mindmapModel.selected);
		mindmapModel.executeAction(action);
   }

	view.searchClicked = function() {
		var texte = prompt("Search key-word :", "type here");
		var node = mindmapModel.getNodeFromCaption(texte);
		var action = new mindmaps.action.centerAction(node);
		mindmapModel.executeAction(action);
	}

    view.delImageClicked = function() {
		var action = new mindmaps.action.linkImageAction(mindmapModel.selected,"none");
		mindmapModel.executeAction(action);
	};


	view.importImageClicked = function(e) {
		var files = e.target.files;
		var file = files[0];
		var reader = new FileReader();
		reader.onload = function() {
			var action = new mindmaps.action.linkImageAction(mindmapModel.selected,reader.result);
			mindmapModel.executeAction(action);
		};
		reader.readAsDataURL(file);
	};

	view.addLinkClicked = function(){
		if(oldNode == null){
			oldNode = mindmapModel.selected;
		}else if(mindmapModel.selected != oldNode) {
			var action = new mindmaps.action.addLinkAction(mindmapModel.selected,oldNode);
			mindmapModel.executeAction(action);
			oldNode = null;
		}
	}

	view.delLinkClicked = function(){
		if(oldNode == null){
			oldNode = mindmapModel.selected;
		}else if(mindmapModel.selected != oldNode) {
			if(mindmapModel.selected.falseChildren.includes(oldNode)) {
				var action = new mindmaps.action.delLinkAction(mindmapModel.selected, mindmapModel.selected.falseChildren.indexOf(oldNode));
			}else{
				var action = new mindmaps.action.delLinkAction(oldNode, oldNode.falseChildren.indexOf(mindmapModel.selected));
			}
			mindmapModel.executeAction(action);
			oldNode = null;
		}
	}

	view.addCommentClicked = function() {
		if(mindmapModel.selected.isRoot) {
			  var action = new mindmaps.action.commentNodeAction(mindmapModel.selected);
			  mindmapModel.executeAction(action);
		}
		else {
			var action = new mindmaps.action.commentLineAction(mindmapModel.selected);
			mindmapModel.executeAction(action);
		}
	}

	view.delCommentClicked = function() {
		if(mindmapModel.selected.isRoot)
		{
		  var action = new mindmaps.action.removeCommentNodeAction(mindmapModel.selected);
		  mindmapModel.executeAction(action);
		}
		else
		{
		  var action = new mindmaps.action.delCommentLineAction(mindmapModel.selected);
		  mindmapModel.executeAction(action);
		}
	}

	view.fontSizeDecreaseButtonClicked = function() {
		if(mindmapModel.selected.isRoot)
		{
			var action = new mindmaps.action.DecreaseNodeFontSizeAction(
				mindmapModel.selected);
			mindmapModel.executeAction(action);
		}
		else {
			var action = new mindmaps.action.DecreaseLineSizeAction(
				mindmapModel.selected);
			mindmapModel.executeAction(action);
		}
	};

  view.fontSizeIncreaseButtonClicked = function() {
	if(mindmapModel.selected.isRoot)
    {
		var action = new mindmaps.action.IncreaseNodeFontSizeAction(
			mindmapModel.selected);
		mindmapModel.executeAction(action);
	}
	else {
		var action = new mindmaps.action.IncreaseLineSizeAction(
			mindmapModel.selected);
		mindmapModel.executeAction(action);
	}
  };

  view.fontBoldCheckboxClicked = function(checked) {
    var action = new mindmaps.action.SetFontWeightAction(
        mindmapModel.selected, checked);
    mindmapModel.executeAction(action);
  };

  view.fontItalicCheckboxClicked = function(checked) {
    var action = new mindmaps.action.SetFontStyleAction(
        mindmapModel.selected, checked);
    mindmapModel.executeAction(action);
  };

  view.fontUnderlineCheckboxClicked = function(checked) {
    var action = new mindmaps.action.SetFontDecorationAction(
        mindmapModel.selected, checked ? "underline" : "none");
    mindmapModel.executeAction(action);
  };

  view.fontLinethroughCheckboxClicked = function(checked) {
    var action = new mindmaps.action.SetFontDecorationAction(
        mindmapModel.selected, checked ? "line-through" : "none");
    mindmapModel.executeAction(action);
  };

  view.branchColorPicked = function(color) {
	  if(mindmapModel.selected.isRoot){
	      var action = new mindmaps.action.SetBranchColorAction(mindmapModel.selected, color);
	  }
	  else {
		  var action = new mindmaps.action.SetBranchColorAction(mindmapModel.selected.getNode(), color, true);
	  }
    mindmapModel.executeAction(action);
  };

  view.branchColorPreview = function(color) {
	  if(mindmapModel.selected.isRoot){
		eventBus.publish(mindmaps.Event.NODE_BRANCH_COLOR_PREVIEW,mindmapModel.selected, color);
	  }
	  else {
		eventBus.publish(mindmaps.Event.NODE_BRANCH_COLOR_PREVIEW, mindmapModel.selected.getNode(), color, true);
	  }

  }

  view.fontColorPicked = function(color) {
    var action = new mindmaps.action.SetFontColorAction(
        mindmapModel.selected, color);
    mindmapModel.executeAction(action);
  };

  view.fontColorPreview = function(color) {
    eventBus.publish(mindmaps.Event.NODE_FONT_COLOR_PREVIEW,
        mindmapModel.selected, color);
  };

  /**white
   * Change branch color of all the node's children.
   */
  view.branchColorChildrenButtonClicked = function() {
    var action = new mindmaps.action.SetChildrenBranchColorAction(
        mindmapModel.selected);
    mindmapModel.executeAction(action);
  }

  /**
   * Update view on font events.
   */
  eventBus.subscribe(mindmaps.Event.NODE_FONT_CHANGED, function(node) {
    if (mindmapModel.selected === node) {
      updateView(node);
    }
  });

  eventBus.subscribe(mindmaps.Event.NODE_BRANCH_COLOR_CHANGED,
      function(node) {
        if (mindmapModel.selected === node) {
          updateView(node);
        }
      });

  eventBus.subscribe(mindmaps.Event.NODE_SELECTED, function(node) {
    updateView(node);
  });

  /**
   * Enable controls when a document has been opened.
   */
  eventBus.subscribe(mindmaps.Event.DOCUMENT_OPENED, function() {
    view.setControlsEnabled(true);
  });

  /**
   * Disable controls when document was closed.
   */
  eventBus.subscribe(mindmaps.Event.DOCUMENT_CLOSED, function() {
    view.setControlsEnabled(false);
  });


  eventBus.subscribe(mindmaps.Event.LINE_SELECTED, function() {
    view.lineSelected();
  });

  eventBus.subscribe(mindmaps.Event.NODE_SELECTED, function() {
    view.nodeSelected();
  });

  /**
   * Sets the view params to match the node's attributes.
   *
   * @param {mindmaps.Node} node
   */
  function updateView(node) {
    var font = node.text.font;
    view.setBoldCheckboxState(font.weight === "bold");
    view.setItalicCheckboxState(font.style === "italic");
    view.setUnderlineCheckboxState(font.decoration === "underline");
    view.setLinethroughCheckboxState(font.decoration === "line-through");
    view.setFontColorPickerColor(font.color);
    view.setBranchColorPickerColor(node.branchColor);
  }

  this.go = function() {
    view.init();
  };
};
