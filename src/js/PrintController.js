/**
 * @constructor
 * @param {mindmaps.EventBus} eventBus
 * @param {mindmaps.CommandRegistry} commandRegistry
 * @param {mindmaps.MindMapModel} mindmapModel
 */
mindmaps.PrintController = function(eventBus, commandRegistry, mindmapModel) {
  var printCommand = commandRegistry.get(mindmaps.PrintCommand);
  printCommand.setHandler(doPrintDocument);

  var renderer = new mindmaps.StaticCanvasRenderer();

  function doPrintDocument() {
    var $img = renderer.renderAsPNG(mindmapModel.getDocument());
    $("#print-area").html($img);
    window.print();

    // TODO chrome only: after print() opens a new tab, and one switches
    // back to the old tab the canvas container has scrolled top-left.
  }

  /**
   * Disable all commands when overview mode is switched on.
   */
  eventBus.subscribe(mindmaps.Event.OVERVIEW_ENABLED, function() {
    commandRegistry.get(mindmaps.PrintCommand).setEnabled(false);
  });

  /**
   * Enable all commands when overview mode is switched off.
   */
  eventBus.subscribe(mindmaps.Event.OVERVIEW_DISABLED, function() {
    commandRegistry.get(mindmaps.PrintCommand).setEnabled(true);
  });
};
